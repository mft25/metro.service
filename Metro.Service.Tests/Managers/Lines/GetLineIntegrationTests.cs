﻿using System.Configuration;
using Flurl;
using Flurl.Http;
using Metro.Service.Managers.Lines;
using Xunit;

namespace Metro.Service.Tests.Managers.Lines
{
    public class GetLineIntegrationTests
    {
        private readonly string _serviceUrl;

        public GetLineIntegrationTests()
        {
            _serviceUrl = ConfigurationManager.AppSettings["ServiceUrl"] + "/api/metro/lines";
        }

        [Fact]
        public async void GetLine_Succeeds()
        {
            var line = await _serviceUrl
                .AppendPathSegment("getline")
                .SetQueryParams(new
                {
                    LineId = 12,
                    Culture = "en-gb"
                })
                .GetJsonAsync<Line>()
                .ConfigureAwait(false);

            Assert.NotNull(line);
            Assert.NotEmpty(line.Stations);
        }
    }
}
