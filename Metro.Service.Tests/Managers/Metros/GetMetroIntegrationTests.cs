﻿using System.Configuration;
using Flurl;
using Flurl.Http;
using Xunit;

namespace Metro.Service.Tests.Managers.Metros
{
    public class GetMetroIntegrationTests
    {
        private readonly string _serviceUrl;

        public GetMetroIntegrationTests()
        {
            _serviceUrl = ConfigurationManager.AppSettings["ServiceUrl"] + "/api/metro/metros";
        }

        [Fact]
        public async void GetMetro_Succeeds()
        {
            var metro = await _serviceUrl
                .AppendPathSegment("getmetro")
                .SetQueryParams(new
                {
                    MetroId = 1,
                    Culture = "en-gb"
                })
                .GetJsonAsync<Service.Managers.Metros.Metro>()
                .ConfigureAwait(false);

            Assert.NotNull(metro);
            Assert.NotEmpty(metro.Lines);
        }
    }
}
