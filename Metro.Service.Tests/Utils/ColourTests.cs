﻿using Metro.Service.Utils;
using Xunit;

namespace Metro.Service.Tests.Utils
{
    public class ColourTests
    {
        [Theory]
        [InlineData("#0088ff", true)] // Canonical colour
        [InlineData("0088ff", true)] // # will be added
        [InlineData("#0088FF", true)] // Will be changed to lower case

        [InlineData("#0088gg", false)] // 'g' is not a hexadecimal character
        [InlineData("0088ff0", false)] // Hex code is too long
        [InlineData("#0088f", false)] // Hex code is too short
        public void Colour_CheckIfStringIsValid(string colourString, bool shouldBeValid)
        {
            var isValid = true;

            try
            {
                Colour colour = colourString;
            }
            catch
            {
                isValid = false;
            }

            Assert.Equal(shouldBeValid, isValid);
        }
    }
}
