﻿using Metro.Service.Utils;
using Xunit;

namespace Metro.Service.Tests.Utils
{
    public class CultureExtensionsTests
    {
        [Theory]
        [InlineData(null, null)]
        [InlineData("ab", null)]
        [InlineData("ab-CD", "ab")]
        [InlineData("ab-CD-Efgh", "ab-CD")]
        public void Culture_ToParentCulture(string culture, string parentCulture)
        {
            Assert.Equal(parentCulture, culture.ToParentCulture());
        }

        [Theory]
        [InlineData("ab", null, 3)]
        [InlineData("ab-CD", "ab", 3)]
        [InlineData("ab-CD", "ab-CD", 4)]
        [InlineData("ab-CD-Efgh", "ab", 2)]
        [InlineData("ab-CD-Efgh", null, 1)]
        [InlineData("ab-CD", "xy", 0)]
        [InlineData("ab-CD", "ab-CD-Efg", 0)]
        public void Culture_MatchLevel_IsMatch(string culture, string resourceCulture, int expectedMatchLevel)
        {
            Assert.Equal(expectedMatchLevel, culture.MatchLevel(resourceCulture));
            Assert.Equal(expectedMatchLevel > 0, culture.IsMatch(resourceCulture));
        }
    }
}
