﻿using System.Web;
using System.Web.Http;
using Castle.Windsor;
using Metro.Service.App_Start;
using Metro.Service.IoC;

namespace Metro.Service
{
    public class Global : HttpApplication
    {
        private static IWindsorContainer _container;

        protected void Application_Start()
        {
            // Windsor
            _container = WindsorConfig.InstallWindsor(
                GlobalConfiguration.Configuration,
                new WindsorInstaller(),
                new Resources.Lines.WindsorInstaller(),
                new Resources.Metros.WindsorInstaller(),
                new Resources.Stations.WindsorInstaller(),
                new Engines.Lines.WindsorInstaller(),
                new Engines.Metros.WindsorInstaller(),
                new Managers.Lines.WindsorInstaller(),
                new Managers.Metros.WindsorInstaller());

            // Web API
            GlobalConfiguration.Configure(WebApiConfig.Register);
        }

        protected void Application_End()
        {
            _container.Dispose();
            Dispose();
        }
    }
}
