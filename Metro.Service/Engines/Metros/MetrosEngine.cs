﻿using System;
using System.Collections.Generic;
using System.Linq;
using Metro.Service.Resources.Lines;
using Metro.Service.Resources.Lines.Contracts;
using Metro.Service.Resources.Metros;
using Metro.Service.Utils.Logging;

namespace Metro.Service.Engines.Metros
{
    public class MetrosEngine : IMetrosEngine
    {
        private readonly ILogger _logger;
        private readonly ILinesResource _linesResource;
        private readonly IMetrosResource _metrosResource;

        public MetrosEngine(
            ILogger logger,
            ILinesResource linesResource,
            IMetrosResource metrosResource)
        {
            _logger = logger;
            _linesResource = linesResource;
            _metrosResource = metrosResource;
        }

        public GetMetroResponse GetMetro(GetMetroRequest request)
        {
            try
            {
                return new GetMetroResponse
                {
                    Status = MetrosEngineStatusCode.Success,

                    Metro = GetEngineMetro(request)
                };
            }
            catch (Exception ex)
            {
                _logger.Log(Level.Error, "Error retrieving Metro", ex);

                var status = MetrosEngineStatusCode.UnknownError;

                if (ex.Data.Contains("status"))
                {
                    status = (MetrosEngineStatusCode)ex.Data["status"];
                }

                return new GetMetroResponse
                {
                    Status = status,
                };
            }
        }

        private Metro GetEngineMetro(GetMetroRequest request)
        {
            var resourceMetro = GetResourceMetro(request);

            var lines = GetLines(resourceMetro.LineIds, request.Culture);

            return new Metro
            {
                MetroId = resourceMetro.MetroId,
                Name = resourceMetro.Name,
                Lines = lines
            };
        }

        private Resources.Metros.Metro GetResourceMetro(GetMetroRequest request)
        {
            var response = _metrosResource.GetMetro(new Resources.Metros.GetMetroRequest
            {
                MetroId = request.MetroId,
                Culture = request.Culture
            });

            if (response.Status != MetrosResourceStatusCode.Success)
            {
                var ex = new Exception("Error occurred when retrieving line data from Lines Resource.");
                ex.Data.Add("status", MetrosEngineStatusCode.MetroRetrievalFailed);
                throw ex;
            }

            return response.Metro;
        }

        private IList<Line> GetLines(IList<int> lineIds, string culture)
        {
            var lines = GetResourceLines(lineIds, culture);

            return lines
                .Select(l => new Line
                {
                    LineId = l.LineId,
                    Position = l.Position,
                    Name = l.Name,
                    Colour = l.Colour,
                    TextColour = l.TextColour
                })
                .ToList();
        }

        private IList<Resources.Lines.Line> GetResourceLines(IList<int> lineIds, string culture)
        {
            var response = _linesResource.GetLines(new GetLinesRequest
            {
                LineIds = lineIds,
                Culture = culture
            });

            if (response.Status != LinesResourceStatusCode.Success)
            {
                var ex = new Exception("Error occurred when retrieving line data from Lines Resource.");
                ex.Data.Add("status", MetrosEngineStatusCode.LineRetrievalFailed);
                throw ex;
            }

            return response.Lines;
        }
    }
}
