﻿using System.Collections.Generic;
using Metro.Service.Utils;

namespace Metro.Service.Engines.Metros
{
    public interface IMetrosEngine
    {
        GetMetroResponse GetMetro(GetMetroRequest request);
    }

    public class GetMetroRequest
    {
        // Key
        public int MetroId { get; set; }

        // Modifiers
        public string Culture { get; set; }
    }

    public class GetMetroResponse
    {
        public MetrosEngineStatusCode Status { get; set; }

        public Metro Metro { get; set; }
    }

    public class Metro
    {
        public int MetroId { get; set; }
        public string Name { get; set; }
        public IList<Line> Lines { get; set; }
    }

    public class Line
    {
        public int LineId { get; set; }
        public int Position { get; set; }
        public string Name { get; set; }
        public Colour Colour { get; set; }
        public Colour TextColour { get; set; }
    }
}
