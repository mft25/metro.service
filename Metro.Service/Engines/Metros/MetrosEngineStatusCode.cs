﻿namespace Metro.Service.Engines.Metros
{
    public enum MetrosEngineStatusCode
    {
        UnknownError = 0,
        Success,
        MetroRetrievalFailed,
        LineRetrievalFailed
    }
}
