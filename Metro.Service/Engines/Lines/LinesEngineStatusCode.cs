﻿namespace Metro.Service.Engines.Lines
{
    public enum LinesEngineStatusCode
    {
        UnknownError = 0,
        Success,
        LineRetrievalFailed,
        MetroRetrievalFailed,
        StationRetrievalFailed
    }
}
