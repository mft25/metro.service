﻿using System;
using Metro.Service.Resources.Metros;

namespace Metro.Service.Engines.Lines.Metros
{
    public interface IMetrosGetter
    {
        Metro GetMetro(int metroId, string culture);
    }

    public class MetrosGetter : IMetrosGetter
    {
        private readonly IMetrosResource _metrosResource;

        public MetrosGetter(IMetrosResource metrosResource)
        {
            _metrosResource = metrosResource;
        }

        public Metro GetMetro(int metroId, string culture)
        {
            var metro = GetResourceMetro(metroId, culture);

            return new Metro
            {
                MetroId = metro.MetroId,
                Name = metro.Name
            };
        }

        private Resources.Metros.Metro GetResourceMetro(int metroId, string culture)
        {
            var response = _metrosResource.GetMetro(new GetMetroRequest
            {
                MetroId = metroId,
                Culture = culture
            });

            if (response.Status != MetrosResourceStatusCode.Success)
            {
                var ex = new Exception("Error occurred when retrieving metro data from Metros Resource.");
                ex.Data.Add("status", LinesEngineStatusCode.MetroRetrievalFailed);
                throw ex;
            }

            return response.Metro;
        }
    }
}
