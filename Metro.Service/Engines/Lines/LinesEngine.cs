﻿using System;
using Metro.Service.Engines.Lines.Metros;
using Metro.Service.Engines.Lines.Stations;
using Metro.Service.Resources.Lines;
using Metro.Service.Utils.Logging;

namespace Metro.Service.Engines.Lines
{
    public class LinesEngine : ILinesEngine
    {
        private readonly ILogger _logger;
        private readonly ILinesResource _linesResource;
        private readonly IMetrosGetter _metrosGetter;
        private readonly IStationsGetter _stationsGetter;

        public LinesEngine(
            ILogger logger,
            ILinesResource linesResource,
            IMetrosGetter metrosGetter,
            IStationsGetter stationsGetter)
        {
            _logger = logger;
            _linesResource = linesResource;
            _metrosGetter = metrosGetter;
            _stationsGetter = stationsGetter;
        }

        public GetLineResponse GetLine(GetLineRequest request)
        {
            try
            {
                return new GetLineResponse
                {
                    Status = LinesEngineStatusCode.Success,

                    Line = GetEngineLine(request)
                };
            }
            catch (Exception ex)
            {
                _logger.Log(Level.Error, "Error retrieving Line", ex);

                var status = LinesEngineStatusCode.UnknownError;

                if (ex.Data.Contains("status"))
                {
                    status = (LinesEngineStatusCode)ex.Data["status"];
                }

                return new GetLineResponse
                {
                    Status = status,
                };
            }
        }

        private Line GetEngineLine(GetLineRequest request)
        {
            var resourceLine = GetResourceLine(request);

            var metro = _metrosGetter.GetMetro(resourceLine.MetroId, request.Culture);

            var stations = _stationsGetter.GetStations(resourceLine.StationIds, request.Culture);

            // Remove the requested line as a connecting line.
            foreach (var station in stations)
            {
                station.ConnectingLines.Remove(request.LineId);
            }

            return new Line
            {
                LineId = resourceLine.LineId,
                Name = resourceLine.Name,
                Colour = resourceLine.Colour,
                TextColour = resourceLine.TextColour,
                Metro = metro,
                Stations = stations
            };
        }

        private Resources.Lines.Line GetResourceLine(GetLineRequest request)
        {
            var response = _linesResource.GetLine(new Resources.Lines.Contracts.GetLineRequest
            {
                LineId = request.LineId,
                Culture = request.Culture
            });

            if (response.Status != LinesResourceStatusCode.Success)
            {
                var ex = new Exception("Error occurred when retrieving line data from Lines Resource.");
                ex.Data.Add("status", LinesEngineStatusCode.LineRetrievalFailed);
                throw ex;
            }

            return response.Line;
        }
    }
}
