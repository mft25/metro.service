﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Metro.Service.Engines.Lines.Metros;
using Metro.Service.Engines.Lines.Stations;

namespace Metro.Service.Engines.Lines
{
    public class WindsorInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Component
                    .For<IMetrosGetter>()
                    .ImplementedBy<MetrosGetter>(),
                Component
                    .For<IStationsGetter>()
                    .ImplementedBy<StationsGetter>());

            container.Register(
                Component
                    .For<ILinesEngine>()
                    .ImplementedBy<LinesEngine>());
        }
    }
}
