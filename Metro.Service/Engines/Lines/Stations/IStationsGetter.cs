﻿using System;
using System.Collections.Generic;
using System.Linq;
using Metro.Service.Resources.Stations;

namespace Metro.Service.Engines.Lines.Stations
{
    public interface IStationsGetter
    {
        IList<Station> GetStations(IList<int> stationIds, string culture);
    }

    public class StationsGetter : IStationsGetter
    {
        private readonly IStationsResource _stationsResource;

        public StationsGetter(IStationsResource stationsResource)
        {
            _stationsResource = stationsResource;
        }

        public IList<Station> GetStations(IList<int> stationIds, string culture)
        {
            var stations = GetResourceStations(stationIds, culture);

            return stations
                .Select(s => new Station
                {
                    StationId = s.StationId,
                    Name = s.Name,
                    ConnectingLines = s.ConnectingLines
                })
                .ToList();
        }

        private IList<Resources.Stations.Station> GetResourceStations(IList<int> stationIds, string culture)
        {
            var response = _stationsResource.GetStations(new GetStationsRequest
            {
                StationIds = stationIds,
                Culture = culture
            });

            if (response.Status != StationsResourceStatusCode.Success)
            {
                var ex = new Exception("Error occurred when retrieving station data from Stations Resource.");
                ex.Data.Add("status", LinesEngineStatusCode.StationRetrievalFailed);
                throw ex;
            }

            return response.Stations;
        }
    }
}
