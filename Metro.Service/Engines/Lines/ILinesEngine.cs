﻿using System.Collections.Generic;
using Metro.Service.Utils;

namespace Metro.Service.Engines.Lines
{
    public interface ILinesEngine
    {
        GetLineResponse GetLine(GetLineRequest request);
    }

    public class GetLineRequest
    {
        // Key
        public int LineId { get; set; }

        // Modifiers
        public string Culture { get; set; }
    }

    public class GetLineResponse
    {
        public LinesEngineStatusCode Status { get; set; }

        public Line Line { get; set; }
    }

    public class Line
    {
        public int LineId { get; set; }
        public string Name { get; set; }
        public Colour Colour { get; set; }
        public Colour TextColour { get; set; }
        public Metro Metro { get; set; }
        public IList<Station> Stations { get; set; }
    }

    public class Metro
    {
        public int MetroId { get; set; }
        public string Name { get; set; }
    }

    public class Station
    {
        public int StationId { get; set; }
        public string Name { get; set; }
        public IList<int> ConnectingLines { get; set; }
    }
}
