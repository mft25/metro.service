﻿using System;

namespace Metro.Service.Utils.Logging
{
    public interface ILogger
    {
        void Log(Level level, string message);
        void Log(Level level, string message, Exception exception);
    }

    public enum Level
    {
        Fatal = 0,
        Error,
        Warning,
        Info,
        Debug
    }
}
