﻿using System;

namespace Metro.Service.Utils.Logging
{
    /// <summary>
    /// Currently there is no logger implementation.
    /// </summary>
    public class Logger : ILogger
    {
        public void Log(Level level, string message)
        {
        }

        public void Log(Level level, string message, Exception exception)
        {
        }
    }
}
