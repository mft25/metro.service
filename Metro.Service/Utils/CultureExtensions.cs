﻿using System;

namespace Metro.Service.Utils
{
    public static class CultureExtensions
    {
        public static string ToParentCulture(this string culture)
        {
            if (culture == null)
            {
                return null;
            }

            var lastHyphenIndex = culture.LastIndexOf("-");
            if (lastHyphenIndex < 0)
            {
                return null;
            }

            return culture.Substring(0, lastHyphenIndex);
        }

        /// <summary>
        /// Get a relative value for how good a given culture matches the culture of a resource.
        /// This value should only be compared when the input <paramref name="culture"/> remains constant.
        /// </summary>
        public static int MatchLevel(this string culture, string resourceCulture)
        {
            var matchLevel = 4;

            while (matchLevel > 0)
            {
                if (culture == null)
                {
                    return resourceCulture == null
                        ? matchLevel
                        : 0;
                }

                if (culture.Equals(resourceCulture, StringComparison.InvariantCultureIgnoreCase))
                {
                    return matchLevel;
                }

                matchLevel--;
                culture = culture.ToParentCulture();
            }

            return 0;
        }

        public static bool IsMatch(this string culture, string resourceCulture)
        {
            return culture.MatchLevel(resourceCulture) > 0;
        }
    }
}
