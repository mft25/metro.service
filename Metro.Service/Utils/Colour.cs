﻿using System;
using System.Text.RegularExpressions;
using Newtonsoft.Json;

namespace Metro.Service.Utils
{
    [JsonConverter(typeof(ToStringJsonConverter))]
    public class Colour
    {
        private readonly string _colour;

        public Colour(string colour)
        {
            colour = colour.ToLowerInvariant();

            if (colour.Length == 6)
            {
                colour = "#" + colour;
            }

            var regex = new Regex(@"^#[0-9a-f]{6}$");
            if (!regex.IsMatch(colour))
            {
                throw new ArgumentException("A colour must be of the form '#123def'");
            }

            _colour = colour;
        }

        public override string ToString()
        {
            return _colour;
        }

        public static implicit operator string(Colour colour)
        {
            return colour.ToString();
        }

        public static implicit operator Colour(string colour)
        {
            return new Colour(colour);
        }
    }
}
