﻿using System.Linq;
using Metro.Service.Resources.Stations.Lines;
using Metro.Service.Resources.Stations.Translations;

namespace Metro.Service.Resources.Stations
{
    public class StationsResource : IStationsResource
    {
        private readonly ITranslationsCache _translationsCache;
        private readonly ILinesCache _linesCache;

        public StationsResource(ITranslationsCache translationsCache, ILinesCache linesCache)
        {
            _translationsCache = translationsCache;
            _linesCache = linesCache;
        }

        public GetStationsResponse GetStations(GetStationsRequest request)
        {
            var stations = request.StationIds
                .Select(s => GetStation(s, request.Culture))
                .ToList();

            return new GetStationsResponse
            {
                Status = StationsResourceStatusCode.Success,

                Stations = stations
            };
        }

        private Station GetStation(int stationId, string culture)
        {
            var name = _translationsCache.GetName(stationId, culture);
            var connectingLines = _linesCache.GetLines(stationId);

            return new Station
            {
                StationId = stationId,
                Name = name,
                ConnectingLines = connectingLines
            };
        }
    }
}
