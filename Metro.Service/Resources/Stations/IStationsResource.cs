﻿using System.Collections.Generic;

namespace Metro.Service.Resources.Stations
{
    public interface IStationsResource
    {
        GetStationsResponse GetStations(GetStationsRequest request);
    }

    public class GetStationsRequest
    {
        // Key
        public IList<int> StationIds { get; set; }

        // Modifiers
        public string Culture { get; set; }
    }

    public class GetStationsResponse
    {
        public StationsResourceStatusCode Status { get; set; }

        public IList<Station> Stations { get; set; }
    }

    public class Station
    {
        public int StationId { get; set; }
        public string Name { get; set; }
        public IList<int> ConnectingLines { get; set; }
    }
}
