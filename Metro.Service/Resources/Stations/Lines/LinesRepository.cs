﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Dapper;

namespace Metro.Service.Resources.Stations.Lines
{
    public class LinesRepository : ILinesRepository
    {
        private readonly string _connectionString;

        public LinesRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public IList<StationLineData> GetLines()
        {
            const string sql = @"
SELECT
    StationId,
    LineId
FROM
    LineStations
";

            using (var connection = new SqlConnection(_connectionString))
            {
                return connection
                    .Query<StationLineData>(sql)
                    .ToList();
            }
        }
    }
}
