﻿using System.Collections.Generic;

namespace Metro.Service.Resources.Stations.Lines
{
    public interface ILinesCache
    {
        IList<int> GetLines(int stationId);
    }
}
