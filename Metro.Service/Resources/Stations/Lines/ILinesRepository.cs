﻿using System.Collections.Generic;

namespace Metro.Service.Resources.Stations.Lines
{
    public interface ILinesRepository
    {
        IList<StationLineData> GetLines();
    }

    public class StationLineData
    {
        public int LineId { get; set; }
        public int StationId { get; set; }
    }
}
