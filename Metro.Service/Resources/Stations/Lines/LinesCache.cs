﻿using System.Collections.Generic;
using System.Linq;

namespace Metro.Service.Resources.Stations.Lines
{
    public class LinesCache : ILinesCache
    {
        private readonly ILinesRepository _linesRepository;
        private readonly ILookup<int, int> _stationsLines;

        public LinesCache(ILinesRepository linesRepository)
        {
            _linesRepository = linesRepository;

            // @@@ This should take place periodically - currently the cache will never update.
            _stationsLines = _linesRepository.GetLines()
                .ToLookup(
                    l => l.StationId,
                    l => l.LineId);
        }

        public IList<int> GetLines(int stationId)
        {
            return _stationsLines[stationId].ToList();
        }
    }
}
