﻿namespace Metro.Service.Resources.Stations.Translations
{
    public interface ITranslationsCache
    {
        string GetName(int stationId, string culture);
    }
}
