﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Dapper;

namespace Metro.Service.Resources.Stations.Translations
{
    public class TranslationsRepository : ITranslationsRepository
    {
        private readonly string _connectionString;

        public TranslationsRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public IList<TranslationData> GetTranslations()
        {
            const string sql = @"
SELECT
    StationId,
    Culture,
    Name
FROM
    StationTranslations
";

            using (var connection = new SqlConnection(_connectionString))
            {
                return connection
                    .Query<TranslationData>(sql)
                    .ToList();
            }
        }
    }
}
