﻿using System.Collections.Generic;

namespace Metro.Service.Resources.Stations.Translations
{
    public interface ITranslationsRepository
    {
        IList<TranslationData> GetTranslations();
    }

    public class TranslationData
    {
        public int StationId { get; set; }
        public string Culture { get; set; }
        public string Name { get; set; }
    }
}
