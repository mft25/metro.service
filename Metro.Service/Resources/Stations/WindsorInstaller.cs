﻿using System.Configuration;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Metro.Service.Resources.Stations.Lines;
using Metro.Service.Resources.Stations.Translations;

namespace Metro.Service.Resources.Stations
{
    public class WindsorInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Component
                    .For<ILinesCache>()
                    .ImplementedBy<LinesCache>(),
                Component
                    .For<ILinesRepository>()
                    .ImplementedBy<LinesRepository>()
                    .DependsOn(Dependency.OnValue(
                        "connectionString", ConfigurationManager.ConnectionStrings["Database"].ConnectionString)),
                Component
                    .For<ITranslationsCache>()
                    .ImplementedBy<TranslationsCache>(),
                Component
                    .For<ITranslationsRepository>()
                    .ImplementedBy<TranslationsRepository>()
                    .DependsOn(Dependency.OnValue(
                        "connectionString", ConfigurationManager.ConnectionStrings["Database"].ConnectionString)),
                Component
                    .For<IStationsResource>()
                    .ImplementedBy<StationsResource>());
        }
    }
}
