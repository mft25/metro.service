﻿using System.Configuration;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Metro.Service.Resources.Lines.Data.Lines;
using Metro.Service.Resources.Lines.Data.Stations;
using Metro.Service.Resources.Lines.Data.Translations;

namespace Metro.Service.Resources.Lines
{
    public class WindsorInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            // Resources
            container.Register(
                Component
                    .For<ILinesCache>()
                    .ImplementedBy<LinesCache>(),
                Component
                    .For<ILinesRepository>()
                    .ImplementedBy<LinesRepository>()
                    .DependsOn(Dependency.OnValue(
                        "connectionString", ConfigurationManager.ConnectionStrings["Database"].ConnectionString)),
                Component
                    .For<ITranslationsCache>()
                    .ImplementedBy<TranslationsCache>(),
                Component
                    .For<ITranslationsRepository>()
                    .ImplementedBy<TranslationsRepository>()
                    .DependsOn(Dependency.OnValue(
                        "connectionString", ConfigurationManager.ConnectionStrings["Database"].ConnectionString)),
                Component
                    .For<IStationsCache>()
                    .ImplementedBy<StationsCache>(),
                Component
                    .For<IStationsRepository>()
                    .ImplementedBy<StationsRepository>()
                    .DependsOn(Dependency.OnValue(
                        "connectionString", ConfigurationManager.ConnectionStrings["Database"].ConnectionString)),
                Component
                    .For<ILinesResource>()
                    .ImplementedBy<LinesResource>());
        }
    }
}
