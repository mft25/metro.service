﻿using System.Linq;
using Metro.Service.Resources.Lines.Contracts;
using Metro.Service.Resources.Lines.Data.Lines;
using Metro.Service.Resources.Lines.Data.Stations;
using Metro.Service.Resources.Lines.Data.Translations;

namespace Metro.Service.Resources.Lines
{
    public class LinesResource : ILinesResource
    {
        private readonly ILinesCache _linesCache;
        private readonly ITranslationsCache _translationsCache;
        private readonly IStationsCache _stationsCache;

        public LinesResource(
            ILinesCache linesCache,
            ITranslationsCache translationsCache,
            IStationsCache stationsCache)
        {
            _linesCache = linesCache;
            _translationsCache = translationsCache;
            _stationsCache = stationsCache;
        }

        public GetLineResponse GetLine(GetLineRequest request)
        {
            var line = GetLine(request.LineId, request.Culture);

            return new GetLineResponse
            {
                Status = LinesResourceStatusCode.Success,

                Line = line
            };
        }

        public GetLinesResponse GetLines(GetLinesRequest request)
        {
            var lines = request.LineIds
                .Select(l => GetLine(l, request.Culture))
                .ToList();

            return new GetLinesResponse
            {
                Status = LinesResourceStatusCode.Success,

                Lines = lines
            };
        }

        private Line GetLine(int lineId, string culture)
        {
            var line = _linesCache.GetLine(lineId);
            var name = _translationsCache.GetName(lineId, culture);
            var stationIds = _stationsCache.GetStations(lineId);

            return new Line
            {
                LineId = line.LineId,
                MetroId = line.MetroId,
                Position = line.Position,
                Name = name,
                Colour = line.Colour,
                TextColour = line.TextColour,
                StationIds = stationIds
            };
        }
    }
}
