﻿using Metro.Service.Resources.Lines.Contracts;

namespace Metro.Service.Resources.Lines
{
    public interface ILinesResource
    {
        GetLineResponse GetLine(GetLineRequest request);
        GetLinesResponse GetLines(GetLinesRequest request);
    }
}
