﻿using System.Collections.Generic;

namespace Metro.Service.Resources.Lines.Contracts
{
    public class GetLinesRequest
    {
        // Key
        public IList<int> LineIds { get; set; }

        // Modifiers
        public string Culture { get; set; }
    }

    public class GetLinesResponse
    {
        public LinesResourceStatusCode Status { get; set; }

        public IList<Line> Lines { get; set; }
    }
}
