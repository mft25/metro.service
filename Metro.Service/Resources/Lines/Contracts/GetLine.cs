﻿namespace Metro.Service.Resources.Lines.Contracts
{
    public class GetLineRequest
    {
        // Key
        public int LineId { get; set; }

        // Modifiers
        public string Culture { get; set; }
    }

    public class GetLineResponse
    {
        public LinesResourceStatusCode Status { get; set; }

        public Line Line { get; set; }
    }
}
