﻿using System.Collections.Generic;
using Metro.Service.Utils;

namespace Metro.Service.Resources.Lines
{
    public class Line
    {
        public int LineId { get; set; }
        public int MetroId { get; set; }
        public int Position { get; set; }
        public string Name { get; set; }
        public Colour Colour { get; set; }
        public Colour TextColour { get; set; }
        public IList<int> StationIds { get; set; }
    }
}
