﻿using System.Linq;
using Metro.Service.Utils;

namespace Metro.Service.Resources.Lines.Data.Translations
{
    public class TranslationsCache : ITranslationsCache
    {
        private readonly ITranslationsRepository _translationsRepository;
        private readonly ILookup<int, TranslationData> _translations;

        public TranslationsCache(ITranslationsRepository translationsRepository)
        {
            _translationsRepository = translationsRepository;

            // @@@ This should take place periodically - currently the cache will never update.
            _translations = _translationsRepository.GetTranslations()
                .ToLookup(t => t.LineId);
        }

        public string GetName(int lineId, string culture)
        {
            var translations = _translations[lineId];

            return translations
                .Where(t => culture.IsMatch(t.Culture))
                .OrderByDescending(t => culture.MatchLevel(t.Culture))
                .First()
                .Name;
        }
    }
}
