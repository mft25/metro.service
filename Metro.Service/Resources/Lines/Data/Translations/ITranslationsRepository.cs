﻿using System.Collections.Generic;

namespace Metro.Service.Resources.Lines.Data.Translations
{
    public interface ITranslationsRepository
    {
        IList<TranslationData> GetTranslations();
    }

    public class TranslationData
    {
        public int LineId { get; set; }
        public string Culture { get; set; }
        public string Name { get; set; }
    }
}
