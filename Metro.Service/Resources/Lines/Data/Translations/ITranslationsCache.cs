﻿namespace Metro.Service.Resources.Lines.Data.Translations
{
    public interface ITranslationsCache
    {
        string GetName(int lineId, string culture);
    }
}
