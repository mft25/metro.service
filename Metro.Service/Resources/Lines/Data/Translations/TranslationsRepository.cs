﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Dapper;

namespace Metro.Service.Resources.Lines.Data.Translations
{
    public class TranslationsRepository : ITranslationsRepository
    {
        private readonly string _connectionString;

        public TranslationsRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public IList<TranslationData> GetTranslations()
        {
            const string sql = @"
SELECT
    LineId,
    Culture,
    Name
FROM
    LineTranslations
";

            using (var connection = new SqlConnection(_connectionString))
            {
                return connection
                    .Query<TranslationData>(sql)
                    .ToList();
            }
        }
    }
}
