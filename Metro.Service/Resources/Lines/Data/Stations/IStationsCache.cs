﻿using System.Collections.Generic;

namespace Metro.Service.Resources.Lines.Data.Stations
{
    public interface IStationsCache
    {
        IList<int> GetStations(int lineId);
    }
}
