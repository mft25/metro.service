﻿using System.Collections.Generic;
using System.Linq;

namespace Metro.Service.Resources.Lines.Data.Stations
{
    public class StationsCache : IStationsCache
    {
        private readonly IStationsRepository _stationsRepository;
        private readonly Dictionary<int, List<int>> _lineStations;

        public StationsCache(IStationsRepository stationsRepository)
        {
            _stationsRepository = stationsRepository;

            // @@@ This should take place periodically - currently the cache will never update.
            _lineStations = _stationsRepository.GetStations()
                .GroupBy(s => s.LineId)
                .ToDictionary(
                    g => g.Key,
                    g => g
                        .OrderBy(ls => ls.Position)
                        .Select(ls => ls.StationId)
                        .ToList());
        }

        public IList<int> GetStations(int lineId)
        {
            List<int> stations;

            if (!_lineStations.TryGetValue(lineId, out stations))
            {
                return new List<int>();
            }

            return stations;
        }
    }
}
