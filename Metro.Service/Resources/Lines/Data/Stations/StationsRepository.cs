﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Dapper;

namespace Metro.Service.Resources.Lines.Data.Stations
{
    public class StationsRepository : IStationsRepository
    {
        private readonly string _connectionString;

        public StationsRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public IList<LineStationData> GetStations()
        {
            const string sql = @"
SELECT
    LineId,
    StationId,
    Position
FROM
    LineStations
";

            using (var connection = new SqlConnection(_connectionString))
            {
                return connection
                    .Query<LineStationData>(sql)
                    .ToList();
            }
        }
    }
}
