﻿using System.Collections.Generic;

namespace Metro.Service.Resources.Lines.Data.Stations
{
    public interface IStationsRepository
    {
        IList<LineStationData> GetStations();
    }

    public class LineStationData
    {
        public int LineId { get; set; }
        public int StationId { get; set; }
        public int Position { get; set; }
    }
}
