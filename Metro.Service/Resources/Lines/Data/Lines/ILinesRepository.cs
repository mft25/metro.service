﻿using System.Collections.Generic;
using Metro.Service.Utils;

namespace Metro.Service.Resources.Lines.Data.Lines
{
    public interface ILinesRepository
    {
        IList<LineData> GetLines();
    }

    public class LineData
    {
        public int LineId { get; set; }
        public int MetroId { get; set; }
        public int Position { get; set; }
        public Colour Colour { get; set; }
        public Colour TextColour { get; set; }
    }
}
