﻿namespace Metro.Service.Resources.Lines.Data.Lines
{
    public interface ILinesCache
    {
        LineData GetLine(int lineId);
    }
}
