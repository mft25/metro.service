﻿using System.Collections.Generic;
using System.Linq;

namespace Metro.Service.Resources.Lines.Data.Lines
{
    public class LinesCache : ILinesCache
    {
        private readonly ILinesRepository _linesRepository;
        private readonly List<LineData> _lines;

        public LinesCache(ILinesRepository linesRepository)
        {
            _linesRepository = linesRepository;

            // @@@ This should take place periodically - currently the cache will never update.
            _lines = _linesRepository.GetLines().ToList();
        }

        public LineData GetLine(int lineId)
        {
            return _lines.Single(l => l.LineId == lineId);
        }
    }
}
