﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Dapper;

namespace Metro.Service.Resources.Lines.Data.Lines
{
    public class LinesRepository : ILinesRepository
    {
        private readonly string _connectionString;

        public LinesRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public IList<LineData> GetLines()
        {
            const string sql = @"
SELECT
    LineId,
    MetroId,
    Position,
    Colour,
    TextColour
FROM
    Lines
";

            using (var connection = new SqlConnection(_connectionString))
            {
                return connection
                    .Query<LineData>(sql)
                    .ToList();
            }
        }
    }
}
