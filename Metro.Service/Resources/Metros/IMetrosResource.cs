﻿using System.Collections.Generic;

namespace Metro.Service.Resources.Metros
{
    public interface IMetrosResource
    {
        GetMetroResponse GetMetro(GetMetroRequest request);
    }

    public class GetMetroRequest
    {
        // Key
        public int MetroId { get; set; }

        // Modifiers
        public string Culture { get; set; }
    }

    public class GetMetroResponse
    {
        public MetrosResourceStatusCode Status { get; set; }

        public Metro Metro { get; set; }
    }

    public class Metro
    {
        public int MetroId { get; set; }
        public string Name { get; set; }
        public IList<int> LineIds { get; set; }
        public IList<int> StationIds { get; set; }
    }
}
