﻿using System.Collections.Generic;

namespace Metro.Service.Resources.Metros.Lines
{
    public interface ILinesRepository
    {
        IList<MetroLineData> GetLines();
    }

    public class MetroLineData
    {
        public int MetroId { get; set; }
        public int LineId { get; set; }
    }
}
