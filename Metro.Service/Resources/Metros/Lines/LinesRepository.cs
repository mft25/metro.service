﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Dapper;

namespace Metro.Service.Resources.Metros.Lines
{
    public class LinesRepository : ILinesRepository
    {
        private readonly string _connectionString;

        public LinesRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public IList<MetroLineData> GetLines()
        {
            const string sql = @"
SELECT
    MetroId,
    LineId
FROM
    Lines
";

            using (var connection = new SqlConnection(_connectionString))
            {
                return connection
                    .Query<MetroLineData>(sql)
                    .ToList();
            }
        }
    }
}
