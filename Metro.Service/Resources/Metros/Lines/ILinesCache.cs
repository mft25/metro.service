﻿using System.Collections.Generic;

namespace Metro.Service.Resources.Metros.Lines
{
    public interface ILinesCache
    {
        IList<int> GetLines(int metroId);
    }
}
