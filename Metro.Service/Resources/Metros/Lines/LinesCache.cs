﻿using System.Collections.Generic;
using System.Linq;

namespace Metro.Service.Resources.Metros.Lines
{
    public class LinesCache : ILinesCache
    {
        private readonly ILinesRepository _linesRepository;
        private readonly ILookup<int, int> _metroLines;

        public LinesCache(ILinesRepository linesRepository)
        {
            _linesRepository = linesRepository;

            // @@@ This should take place periodically - currently the cache will never update.
            _metroLines = _linesRepository.GetLines()
                .ToLookup(
                    l => l.MetroId,
                    l => l.LineId);
        }

        public IList<int> GetLines(int metroId)
        {
            return _metroLines[metroId].ToList();
        }
    }
}
