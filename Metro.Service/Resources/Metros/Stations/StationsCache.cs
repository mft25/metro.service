﻿using System.Collections.Generic;
using System.Linq;

namespace Metro.Service.Resources.Metros.Stations
{
    public class StationsCache : IStationsCache
    {
        private readonly IStationsRepository _stationsRepository;
        private readonly ILookup<int, int> _metroStations;

        public StationsCache(IStationsRepository stationsRepository)
        {
            _stationsRepository = stationsRepository;

            // @@@ This should take place periodically - currently the cache will never update.
            _metroStations = _stationsRepository.GetStations()
                .ToLookup(
                    l => l.MetroId,
                    l => l.StationId);
        }

        public IList<int> GetStations(int metroId)
        {
            return _metroStations[metroId].ToList();
        }
    }
}
