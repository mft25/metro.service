﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Dapper;

namespace Metro.Service.Resources.Metros.Stations
{
    public class StationsRepository : IStationsRepository
    {
        private readonly string _connectionString;

        public StationsRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public IList<MetroStationData> GetStations()
        {
            const string sql = @"
SELECT
    MetroId,
    StationId
FROM
    Stations
";

            using (var connection = new SqlConnection(_connectionString))
            {
                return connection
                    .Query<MetroStationData>(sql)
                    .ToList();
            }
        }
    }
}
