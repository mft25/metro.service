﻿using System.Collections.Generic;

namespace Metro.Service.Resources.Metros.Stations
{
    public interface IStationsRepository
    {
        IList<MetroStationData> GetStations();
    }

    public class MetroStationData
    {
        public int MetroId { get; set; }
        public int StationId { get; set; }
    }
}
