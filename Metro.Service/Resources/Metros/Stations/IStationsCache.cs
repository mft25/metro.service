﻿using System.Collections.Generic;

namespace Metro.Service.Resources.Metros.Stations
{
    public interface IStationsCache
    {
        IList<int> GetStations(int metroId);
    }
}
