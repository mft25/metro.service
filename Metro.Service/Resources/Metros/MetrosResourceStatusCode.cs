﻿namespace Metro.Service.Resources.Metros
{
    public enum MetrosResourceStatusCode
    {
        UnknownError = 0,
        Success
    }
}
