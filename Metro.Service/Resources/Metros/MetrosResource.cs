﻿using Metro.Service.Resources.Metros.Lines;
using Metro.Service.Resources.Metros.Stations;
using Metro.Service.Resources.Metros.Translations;

namespace Metro.Service.Resources.Metros
{
    public class MetrosResource : IMetrosResource
    {
        private readonly ITranslationsCache _translationsCache;
        private readonly ILinesCache _linesCache;
        private readonly IStationsCache _stationsCache;

        public MetrosResource(
            ITranslationsCache translationsCache,
            ILinesCache linesCache,
            IStationsCache stationsCache)
        {
            _translationsCache = translationsCache;
            _linesCache = linesCache;
            _stationsCache = stationsCache;
        }

        public GetMetroResponse GetMetro(GetMetroRequest request)
        {
            var name = _translationsCache.GetName(request.MetroId, request.Culture);
            var lineIds = _linesCache.GetLines(request.MetroId);
            var stationIds = _stationsCache.GetStations(request.MetroId);

            return new GetMetroResponse
            {
                Status = MetrosResourceStatusCode.Success,

                Metro = new Metro
                {
                    MetroId = request.MetroId,
                    Name = name,
                    LineIds = lineIds,
                    StationIds = stationIds
                }
            };
        }
    }
}
