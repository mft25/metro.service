﻿namespace Metro.Service.Resources.Metros.Translations
{
    public interface ITranslationsCache
    {
        string GetName(int metroId, string culture);
    }
}
