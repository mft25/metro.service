﻿using System.Collections.Generic;

namespace Metro.Service.Resources.Metros.Translations
{
    public interface ITranslationsRepository
    {
        IList<TranslationData> GetTranslations();
    }

    public class TranslationData
    {
        public int MetroId { get; set; }
        public string Culture { get; set; }
        public string Name { get; set; }
    }
}
