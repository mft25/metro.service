﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Dapper;

namespace Metro.Service.Resources.Metros.Translations
{
    public class TranslationsRepository : ITranslationsRepository
    {
        private readonly string _connectionString;

        public TranslationsRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public IList<TranslationData> GetTranslations()
        {
            const string sql = @"
SELECT
    MetroId,
    Culture,
    Name
FROM
    MetroTranslations
";

            using (var connection = new SqlConnection(_connectionString))
            {
                return connection
                    .Query<TranslationData>(sql)
                    .ToList();
            }
        }
    }
}
