﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Metro.Service.Utils;
using Metro.Service.Utils.Logging;

namespace Metro.Service.IoC
{
    public class WindsorInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Component
                    .For<ILogger>()
                    .ImplementedBy<Logger>(),
                Component
                    .For<DocumentationController>()
                    .LifestyleTransient());
        }
    }
}
