﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;

namespace Metro.Service.Managers.Lines
{
    public class WindsorInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            // Managers
            container.Register(
                Component
                    .For<GetLineController>()
                    .LifestyleTransient());
        }
    }
}
