﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using Metro.Service.Engines.Lines;
using Metro.Service.Utils;

namespace Metro.Service.Managers.Lines
{
    [RoutePrefix("api/metro/lines")]
    public class GetLineController : ApiController
    {
        private readonly ILinesEngine _linesEngine;

        public GetLineController(ILinesEngine linesEngine)
        {
            _linesEngine = linesEngine;
        }

        [HttpGet]
        [Route("getline")]
        [EnableCors("*", "*", "*")]
        [ResponseType(typeof(Line))]
        public IHttpActionResult GetLine([FromUri]GetLineRequest request)
        {
            // Manager should also be responsible for validation, HTTP response codes, etc.
            // At the moment it is pretty much just a wrapper.
            var response = _linesEngine.GetLine(new Engines.Lines.GetLineRequest
            {
                LineId = request.LineId,
                Culture = request.Culture
            });

            if (response.Status != LinesEngineStatusCode.Success)
            {
                throw new HttpResponseException(HttpStatusCode.InternalServerError);
            }

            return Ok(new Line(response.Line));
        }
    }

    public class GetLineRequest
    {
        // Key
        public int LineId { get; set; }

        // Modifiers
        public string Culture { get; set; }
    }

    public class Line
    {
        public int LineId { get; set; }
        public string Name { get; set; }
        public Colour Colour { get; set; }
        public Colour TextColour { get; set; }
        public Metro Metro { get; set; }
        public IList<Station> Stations { get; set; }

        public Line()
        {
        }

        public Line(Engines.Lines.Line engineLine)
        {
            LineId = engineLine.LineId;
            Name = engineLine.Name;
            Colour = engineLine.Colour;
            TextColour = engineLine.TextColour;
            Metro = new Metro(engineLine.Metro);
            Stations = engineLine.Stations
                .Select(s => new Station(s))
                .ToList();
        }
    }

    public class Metro
    {
        public int MetroId { get; set; }
        public string Name { get; set; }

        public Metro()
        {
        }

        public Metro(Engines.Lines.Metro engineMetro)
        {
            MetroId = engineMetro.MetroId;
            Name = engineMetro.Name;
        }
    }

    public class Station
    {
        public int StationId { get; set; }
        public string Name { get; set; }
        public IList<int> ConnectingLines { get; set; }

        public Station()
        {
        }

        public Station(Engines.Lines.Station engineStation)
        {
            StationId = engineStation.StationId;
            Name = engineStation.Name;
            ConnectingLines = engineStation.ConnectingLines;
        }
    }
}
