﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;

namespace Metro.Service.Managers.Metros
{
    public class WindsorInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            // Managers
            container.Register(
                Component
                    .For<GetMetroController>()
                    .LifestyleTransient());
        }
    }
}
