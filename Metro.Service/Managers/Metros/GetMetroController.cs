﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using Metro.Service.Engines.Metros;
using Metro.Service.Utils;

namespace Metro.Service.Managers.Metros
{
    [RoutePrefix("api/metro/metros")]
    public class GetMetroController : ApiController
    {
        private readonly IMetrosEngine _metrosEngine;

        public GetMetroController(IMetrosEngine metrosEngine)
        {
            _metrosEngine = metrosEngine;
        }

        [HttpGet]
        [Route("getmetro")]
        [EnableCors("*", "*", "*")]
        [ResponseType(typeof(Metro))]
        public IHttpActionResult GetLine([FromUri]GetMetroRequest request)
        {
            // Manager should also be responsible for validation, HTTP response codes, etc.
            // At the moment it is pretty much just a wrapper.
            var response = _metrosEngine.GetMetro(new Engines.Metros.GetMetroRequest
            {
                MetroId = request.MetroId,
                Culture = request.Culture
            });

            if (response.Status != MetrosEngineStatusCode.Success)
            {
                throw new HttpResponseException(HttpStatusCode.InternalServerError);
            }

            return Ok(new Metro(response.Metro));
        }
    }

    public class GetMetroRequest
    {
        // Key
        public int MetroId { get; set; }

        // Modifiers
        public string Culture { get; set; }
    }

    public class Metro
    {
        public int MetroId { get; set; }
        public string Name { get; set; }
        public IList<Line> Lines { get; set; }

        public Metro()
        {
        }

        public Metro(Engines.Metros.Metro engineMetro)
        {
            MetroId = engineMetro.MetroId;
            Name = engineMetro.Name;
            Lines = engineMetro.Lines
                .OrderBy(l => l.Position)
                .ThenBy(l => l.Name)
                .Select(l => new Line(l))
                .ToList();
        }
    }

    public class Line
    {
        public int LineId { get; set; }
        public string Name { get; set; }
        public Colour Colour { get; set; }
        public Colour TextColour { get; set; }

        public Line()
        {
        }

        public Line(Engines.Metros.Line engineLine)
        {
            LineId = engineLine.LineId;
            Name = engineLine.Name;
            Colour = engineLine.Colour;
            TextColour = engineLine.TextColour;
        }
    }
}
